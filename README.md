# Moovi

> Moovi is a simple example of react native application that provides information about your favorite movie

## Used in this project

- React Navigation [(read docs)](https://reactnavigation.org/)
- Redux [(read docs)](https://react-redux.js.org/introduction/quick-start)
- Axios [(read docs)](https://github.com/axios/axios)
- Custom Components
- ... and others

# App Preview

This is the link to download a preview of the Moovi app.

[Download APK](https://drive.google.com/file/d/1Wyg0r0w4UnSIA7x6yzw_uRB856t4nqyk/view?usp=sharing)

or you can also see some screenshots of the app here

<p align="center">
<img src="/uploads/ac79d1002e9a69f204cab1fcff99e24d/moovi-search.jpg" width="200" /> <img src="/uploads/6abf58daf58b65aff050a815e3917f29/moovi-home.jpg" width="200" /> <img src="/uploads/aea523ae634b81b407bc07327a321ccc/moovi-detail.jpg" width="200" />
</p>
