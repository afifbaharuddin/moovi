import {scaleFont} from './Mixins';
import {Colors} from '.';

// FONT FAMILY
export const FONT_FAMILY_REGULAR = 'OpenSans-Regular';
export const FONT_FAMILY_BOLD = 'OpenSans-Bold';

// FONT WEIGHT
export const FONT_WEIGHT_REGULAR = '400';
export const FONT_WEIGHT_BOLD = '700';

// FONT SIZE
export const FONT_SIZE_16 = scaleFont(16);
export const FONT_SIZE_14 = scaleFont(14);
export const FONT_SIZE_12 = scaleFont(12);

// LINE HEIGHT
export const LINE_HEIGHT_24 = scaleFont(24);
export const LINE_HEIGHT_20 = scaleFont(20);
export const LINE_HEIGHT_16 = scaleFont(16);

// FONT STYLE
export const FONT_REGULAR = {
  fontFamily: FONT_FAMILY_REGULAR,
  fontWeight: FONT_WEIGHT_REGULAR,
};

export const FONT_BOLD = {
  fontFamily: FONT_FAMILY_BOLD,
  fontWeight: FONT_WEIGHT_BOLD,
};

export const TITLE_BIG = {
  ...FONT_BOLD,
  fontSize: 32,
  color: Colors.PRIMARY,
};

export const TITLE_SMALL = {
  ...FONT_BOLD,
  fontSize: 12,
  color: Colors.PRIMARY,
};

export const TITLE_PRIMARY = {
  ...FONT_BOLD,
  fontSize: 24,
  color: Colors.PRIMARY,
};

export const TITLE = {
  ...FONT_BOLD,
  fontSize: 24,
  color: Colors.GRAY_DARK,
};

export const SUBTITLE = {
  ...FONT_REGULAR,
  fontSize: 16,
  color: Colors.GRAY_DARK,
};

export const SUBTITLE_SMALL = {
  ...FONT_REGULAR,
  fontSize: 12,
  color: Colors.GRAY_DARK,
};

export const CONTENT = {
  ...FONT_REGULAR,
  fontSize: 14,
  color: Colors.GRAY_MEDIUM,
};
