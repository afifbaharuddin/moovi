import {Dimensions, PixelRatio} from 'react-native';
import {Colors} from '.';

const WINDOW_WIDTH = Dimensions.get('window').width;
const guidelineBaseWidth = 375;

export const scaleSize = (size) => (WINDOW_WIDTH / guidelineBaseWidth) * size;

export const scaleFont = (size) => size * PixelRatio.getFontScale();

export function boxShadow(
  radius = 5,
  color = '#000',
  backgroundColor = 'white',
  offset = {height: 2, width: 2},
  opacity = 0.2,
) {
  return {
    shadowColor: color,
    shadowOffset: offset,
    shadowOpacity: opacity,
    shadowRadius: radius,
    elevation: radius,
    backgroundColor: backgroundColor,
  };
}

export const container = {
  flex: 1,
  backgroundColor: Colors.BACKGROUND,
};

export const appbar = {
  ...boxShadow(),
  minHeight: 56,
  flexDirection: 'row',
  alignItems: 'center',
  paddingHorizontal: 16,
  paddingVertical: 8,
};
