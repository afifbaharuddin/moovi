import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Feather';
import {
  Home,
  Login,
  Splash,
  About,
  MovieDetail,
  Account,
  Search,
} from '../screens';
import {Colors} from '../styles';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const TabIcon = (iconName) => ({
  tabBarIcon: ({color}) => <Icon name={iconName} color={color} size={26} />,
});

const Tabs = () => (
  <Tab.Navigator
    backBehavior="initialRoute"
    initialRouteName="Home"
    tabBarOptions={{
      activeTintColor: Colors.PRIMARY,
    }}>
    <Tab.Screen name="Home" component={Home} options={TabIcon('home')} />
    <Tab.Screen name="Search" component={Search} options={TabIcon('search')} />
    <Tab.Screen name="Account" component={Account} options={TabIcon('user')} />
    <Tab.Screen name="About" component={About} options={TabIcon('info')} />
  </Tab.Navigator>
);

const Navigation = () => (
  <NavigationContainer>
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="MovieDetail" component={MovieDetail} />
      <Stack.Screen name="Tabs" component={Tabs} />
    </Stack.Navigator>
  </NavigationContainer>
);

export default Navigation;
