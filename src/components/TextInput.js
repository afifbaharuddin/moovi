import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  TextInput as TextInputBase,
} from 'react-native';
import {Colors} from '../styles';

const TextInput = ({title, ...props}) => {
  return (
    <View>
      {title && <Text style={styles.inputTitle}>{title}</Text>}
      <TextInputBase
        {...props}
        ref={props.cref}
        style={[{...props.style}, styles.inputText]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputTitle: {
    color: Colors.GRAY_DARK,
    fontSize: 12,
    marginBottom: 4,
  },
  inputText: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    borderColor: Colors.GRAY_LIGHT,
    justifyContent: 'center',
    padding: 8,
    fontSize: 16,
    color: Colors.GRAY_DARK,
  },
});

export default TextInput;
