import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Colors} from '../styles';

const Button = ({text, outlined = false, ...props}) => {
  return (
    <TouchableOpacity
      {...props}
      style={outlined ? styles.btnContainerOutlined : styles.btnContainer}>
      <Text
        style={[
          {...props.style},
          outlined ? styles.btnTextOutlined : styles.btnText,
        ]}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btnContainer: {
    padding: 16,
    marginHorizontal: 1,
    backgroundColor: Colors.PRIMARY,
    borderRadius: 8,
  },
  btnText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: 'center',
  },

  btnContainerOutlined: {
    padding: 16,
    backgroundColor: 'white',
    borderColor: Colors.PRIMARY,
    borderWidth: 1,
    borderRadius: 8,
  },
  btnTextOutlined: {
    color: Colors.PRIMARY,
    fontSize: 16,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
});

export default Button;
