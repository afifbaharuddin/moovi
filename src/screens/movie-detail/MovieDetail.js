import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {baseImageURL} from '../../api/Config';
import {boxShadow} from '../../styles/Mixins';
import {Typography, Colors, Mixins} from '../../styles';
import Cast from './Cast';
import APIKit from '../../api/APIKit';
import {ScrollView} from 'react-native-gesture-handler';

export default class MovieDetail extends Component {
  constructor(props) {
    super(props);
    this.movie = this.props.route.params.movie;
    this.state = {
      isLoading: true,
      cast: [],
    };
  }

  componentDidMount() {
    this.getCast();
  }

  getCast = async () => {
    try {
      const response = await Promise.all([
        APIKit.get(`/movie/${this.movie.id}/credits`),
      ]);

      this.setState({
        isLoading: false,
        cast: response[0].data.cast,
      });
    } catch (error) {
      console.log(error);
    }
  };

  iconedTitle = (iconName, title) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginTop: 16,
          alignItems: 'center',
        }}>
        <Icon name={iconName} color={Colors.PRIMARY} size={24} />
        <Text style={{...Typography.TITLE_PRIMARY, marginStart: 16}}>
          {title}
        </Text>
      </View>
    );
  };

  render() {
    return (
      <ScrollView style={Mixins.container}>
        <Image
          style={styles.imageBanner}
          source={{uri: `${baseImageURL}${this.movie.backdrop_path}`}}
        />

        <View style={{flexDirection: 'row'}}>
          <View style={styles.imagePosterContainer}>
            <Image
              style={styles.imagePoster}
              source={{uri: `${baseImageURL}${this.movie.poster_path}`}}
            />
          </View>

          <View style={{flex: 1, marginTop: 8, marginEnd: 16}}>
            <Text style={Typography.TITLE}>{this.movie.title}</Text>
            <Text style={Typography.TITLE_SMALL}>
              {this.movie.genres.map((e) => e.name).join(' | ')}
            </Text>
            <Text
              style={{
                ...Typography.TITLE_SMALL,
                color: Colors.GRAY_DARK,
                marginTop: 4,
              }}>
              {this.movie.release_date}
            </Text>
            {this.iconedTitle('star', this.movie.vote_average)}
          </View>
        </View>

        <Text style={{...Typography.TITLE_PRIMARY, marginStart: 16}}>
          Overview
        </Text>
        <Text style={styles.movieOverview}>{this.movie.overview}</Text>

        <Text
          style={{...Typography.TITLE_PRIMARY, marginStart: 16, marginTop: 8}}>
          The Cast
        </Text>

        <FlatList
          style={{flexGrow: 0}}
          contentContainerStyle={{padding: 8}}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={this.state.cast}
          renderItem={(cast) => <Cast cast={cast.item} />}
          keyExtractor={(item) => String(item.id)}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  imageBanner: {
    width: '100%',
    height: 240,
    resizeMode: 'cover',
  },
  imagePosterContainer: {
    ...boxShadow(),
    width: 160,
    height: 240,
    margin: 16,
    marginTop: -80,
    borderRadius: 5,
  },
  imagePoster: {
    width: 160,
    height: 240,
    borderRadius: 5,
    resizeMode: 'cover',
  },

  movieOverview: {
    ...Typography.CONTENT,
    marginHorizontal: 16,
    marginVertical: 8,
    color: Colors.GRAY_DARK,
  },
});
