import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, Dimensions} from 'react-native';
import {baseImageURL} from '../../api/Config';
import {Colors, Mixins, Typography} from '../../styles';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default Cast = ({cast, ...props}) => {
  return (
    <View {...props} style={[{...props.style}, styles.cardViewSmall]}>
      <Image
        style={styles.cardViewSmallImage}
        source={{uri: `${baseImageURL}${cast.profile_path}`}}
      />
      <Text style={styles.cardViewSmallText}>{cast.name}</Text>
      <Text style={styles.cardViewSmallTextPrimary}>{cast.character}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  cardViewSmall: {
    ...Mixins.boxShadow(),
    maxWidth: 120,
    borderRadius: 5,
    marginHorizontal: 4,
    marginVertical: 8,
  },
  cardViewSmallImage: {
    width: 120,
    height: 120,
    marginBottom: 8,
    resizeMode: 'cover',
    borderRadius: 5,
  },
  cardViewSmallText: {
    ...Typography.CONTENT,
    color: Colors.GRAY_DARK,
    marginHorizontal: 12,
  },
  cardViewSmallTextPrimary: {
    ...Typography.TITLE_SMALL,
    marginHorizontal: 12,
    marginBottom: 8,
  },
});
