import Home from './home/Home';
import Login from './login/Login';
import Splash from './splash/Splash';
import About from './about/About';
import MovieDetail from './movie-detail/MovieDetail';
import Account from './account/Account';
import Search from './search/Search';

export {Login, Home, Splash, About, MovieDetail, Search, Account};
