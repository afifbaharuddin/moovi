import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {Colors, Typography, Mixins} from '../../styles';
import {Button} from '../../components';
import {LoginAction} from '../../redux/actions';
import {setClientToken} from '../../api/APIKit';
import {connect} from 'react-redux';

class Account extends Component {
  onLogout = () => {
    const {dispatch} = this.props;
    dispatch(LoginAction.userLogout());
    setClientToken('');
    this.props.navigation.reset({
      index: 0,
      routes: [{name: 'Login'}],
    });
  };

  render() {
    return (
      <View style={{...Mixins.container, alignItems: 'center'}}>
        <View style={styles.avatar}>
          <Icon color={Colors.PRIMARY} name="user" size={80} />
        </View>

        <Text
          style={{
            ...Typography.TITLE_BIG,
            marginTop: 32,
            marginBottom: 56,
          }}>
          {this.props.login.username}
        </Text>

        <Button text="Log Out" outlined onPress={this.onLogout} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  avatar: {
    marginTop: 80,
    width: 120,
    height: 120,
    borderRadius: 60,
    resizeMode: 'cover',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.GRAY_LIGHT,
  },
});

const mapStateToProps = (state) => ({
  login: state.login,
});

export default connect(mapStateToProps)(Account);
