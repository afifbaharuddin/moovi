import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {Mixins, Colors, Typography} from '../../styles';
import Icon from 'react-native-vector-icons/Feather';

import {name as app_name, version as app_version} from '../../../package.json';
import {ScrollView} from 'react-native-gesture-handler';

const About = () => {
  return (
    <ScrollView
      contentContainerStyle={{
        ...Mixins.container,
        alignItems: 'center',
        padding: 16,
      }}>
      <Icon
        name="film"
        size={80}
        style={{
          color: Colors.PRIMARY,
        }}
      />
      <Text style={Typography.TITLE_PRIMARY}>{app_name}</Text>
      <Text style={Typography.TITLE_SMALL}>{`version ${app_version}`}</Text>
      <Text style={{...Typography.SUBTITLE, marginTop: 80}}>Developed by</Text>
      <Image
        style={styles.imageAbout}
        source={require('../../assets/images/about.jpg')}
      />
      <Text style={{...Typography.TITLE}}>Afif Baharuddin</Text>
      <Text style={{...Typography.SUBTITLE}}>afifbaharuddin.herokuapp.com</Text>
    </ScrollView>
  );
};

export default About;

const styles = StyleSheet.create({
  imageAbout: {
    width: 160,
    height: 160,
    borderRadius: 80,
    resizeMode: 'cover',
    marginTop: 8,
  },
});
