import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {Colors} from '../../styles';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(function () {
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    }, 2000);
  });

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Icon
        name="film"
        size={80}
        style={{
          color: Colors.PRIMARY,
        }}
      />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({});
