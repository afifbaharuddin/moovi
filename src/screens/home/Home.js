import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, FlatList, Alert} from 'react-native';
import {Colors, Mixins, Typography} from '../../styles';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import APIKit from '../../api/APIKit';
import MovieItem from './MovieItem';

const mapStateToProps = (state) => ({
  login: state.login,
});

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      genres: [],
      moviesPopular: [],
      moviesUpcoming: [],
    };
  }

  componentDidMount() {
    this.getMoviesPopular();
  }

  getMoviesPopular = async () => {
    try {
      const response = await Promise.all([
        APIKit.get('/genre/movie/list'),
        APIKit.get('/movie/popular'),
        APIKit.get('/movie/upcoming'),
      ]);

      response[1].data.results.forEach((element, index) => {
        response[1].data.results[index].genres = response[0].data.genres.filter(
          function (item) {
            return element.genre_ids.includes(item.id);
          },
        );
      });

      response[2].data.results.forEach((element, index) => {
        response[2].data.results[index].genres = response[0].data.genres.filter(
          function (item) {
            return element.genre_ids.includes(item.id);
          },
        );
      });

      this.setState({
        isLoading: false,
        genres: response[0].data.genres,
        moviesPopular: response[1].data.results,
        moviesUpcoming: response[2].data.results,
      });
    } catch (error) {
      console.log(error);
    }
  };

  iconedTitle = (iconName, title) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginHorizontal: 16,
          marginTop: 16,
          alignItems: 'center',
        }}>
        <Icon name={iconName} color={Colors.PRIMARY} size={24} />
        <Text style={{...Typography.TITLE_PRIMARY, marginStart: 16}}>
          {title}
        </Text>
      </View>
    );
  };

  render() {
    return (
      <View style={{...Mixins.container}}>
        <View style={{...Mixins.appbar, justifyContent: 'space-between'}}>
          <Text style={{...Typography.TITLE, color: Colors.GRAY_MEDIUM}}>
            Hi,{' '}
          </Text>
          <Text style={{...Typography.TITLE, flexGrow: 1}}>
            {this.props.login.username}
          </Text>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Search')}>
            <Icon
              name="search"
              color={Colors.GRAY_MEDIUM}
              size={24}
              style={{padding: 8}}
            />
          </TouchableOpacity>
        </View>

        {this.state.isLoading ? (
          <Text style={{flexGrow: 1, alignSelf: 'center'}}>Loading</Text>
        ) : (
          <ScrollView nestedScrollEnabled={true}>
            {this.iconedTitle('film', 'Popular Movies')}

            <FlatList
              style={{flexGrow: 0}}
              contentContainerStyle={{padding: 8}}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={this.state.moviesPopular}
              renderItem={(movie) => (
                <MovieItem
                  horizontal
                  movie={movie.item}
                  onPress={() =>
                    this.props.navigation.navigate('MovieDetail', {
                      movie: movie.item,
                    })
                  }
                />
              )}
              keyExtractor={(item) => String(item.id)}
            />

            {this.iconedTitle('film', 'Upcoming')}

            <FlatList
              nestedScrollEnabled={true}
              style={{flexGrow: 0}}
              contentContainerStyle={{padding: 8}}
              numColumns={2}
              showsHorizontalScrollIndicator={false}
              data={this.state.moviesUpcoming}
              renderItem={(movie) => (
                <MovieItem
                  column={2}
                  movie={movie.item}
                  onPress={() =>
                    this.props.navigation.navigate('MovieDetail', {
                      movie: movie.item,
                    })
                  }
                />
              )}
              keyExtractor={(item) => String(item.id)}
            />
          </ScrollView>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
    padding: 16,
  },
});

export default connect(mapStateToProps)(Home);
