import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, Dimensions} from 'react-native';
import {baseImageURL} from '../../api/Config';
import {Colors, Mixins, Typography} from '../../styles';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default MovieItem = ({
  movie,
  column = 1,
  horizontal = false,
  ...props
}) => {
  const width = Dimensions.get('window').width / column - 8 * column;
  const height = width * 1.5;
  return (
    <TouchableOpacity
      {...props}
      style={
        horizontal
          ? styles.cardViewSmall
          : {...styles.cardViewSmall, width: width, maxWidth: width}
      }>
      <Image
        style={
          horizontal
            ? styles.cardViewSmallImage
            : {...styles.cardViewSmallImage, width: '100%', height: height}
        }
        source={{uri: `${baseImageURL}${movie.poster_path}`}}
      />
      <Text
        numberOfLines={1}
        ellipsizeMode="tail"
        style={styles.cardViewSmallText}>
        {movie.title}
      </Text>
      <Text
        numberOfLines={1}
        ellipsizeMode="tail"
        style={styles.cardViewSmallTextPrimary}>
        {movie.genres.map((e) => e.name).join(' | ')}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cardViewSmall: {
    ...Mixins.boxShadow(),
    maxWidth: 160,
    borderRadius: 5,
    marginHorizontal: 4,
    marginVertical: 8,
  },
  cardViewSmallImage: {
    width: 160,
    height: 240,
    marginBottom: 8,
    resizeMode: 'cover',
    borderRadius: 5,
  },
  cardViewSmallText: {
    ...Typography.SUBTITLE,
    marginHorizontal: 12,
  },
  cardViewSmallTextPrimary: {
    ...Typography.TITLE_SMALL,
    marginHorizontal: 12,
    marginBottom: 8,
  },
});
