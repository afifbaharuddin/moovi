import React, {Component} from 'react';
import {Text, StyleSheet, View, FlatList} from 'react-native';
import {Mixins, Typography, Colors} from '../../styles';
import {TextInput} from '../../components';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import APIKit from '../../api/APIKit';

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isError: false,
      errorMessage: '',
      searchKeyword: '',
      genres: [],
      movies: [],
    };
  }

  componentDidMount() {
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.inputSearch.focus();
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onSearchSubmit = () => {
    if (this.state.searchKeyword) {
      this.setState({isLoading: true, isError: false});
      this.searcMovie(this.state.searchKeyword);
    } else {
      this.inputSearch.focus();
    }
  };

  searcMovie = async (keyword) => {
    try {
      const response = await Promise.all([
        APIKit.get('/genre/movie/list'),
        APIKit.get(`/search/movie`, {params: {query: keyword}}),
      ]);

      response[1].data.results.forEach((element, index) => {
        response[1].data.results[index].genres = response[0].data.genres.filter(
          function (item) {
            return element.genre_ids.includes(item.id);
          },
        );
      });

      this.setState({
        isLoading: false,
        movies: response[1].data.results,
      });
    } catch (error) {
      console.log(error);
      this.setState({
        isLoading: false,
        isError: true,
        errorMessage: 'Oops! Something went wrong.',
      });
    }
  };

  render() {
    return (
      <View style={Mixins.container}>
        <View style={{...Mixins.appbar, paddingEnd: 8}}>
          <View style={{flexGrow: 1}}>
            <TextInput
              placeholder="Search Movie"
              cref={(input) => (this.inputSearch = input)}
              returnKeyType="search"
              onChangeText={(text) => this.setState({searchKeyword: text})}
              onSubmitEditing={this.onSearchSubmit}
            />
          </View>

          <TouchableOpacity onPress={this.onSearchSubmit}>
            <Icon
              name="search"
              color={Colors.GRAY_MEDIUM}
              size={24}
              style={{padding: 8}}
            />
          </TouchableOpacity>
        </View>

        {this.state.isLoading && (
          <Text style={{alignSelf: 'center'}}>Loading</Text>
        )}

        {this.state.isError && (
          <Text style={{alignSelf: 'center'}}>{this.state.errorMessage}</Text>
        )}

        <FlatList
          nestedScrollEnabled={true}
          style={{flexGrow: 0}}
          contentContainerStyle={{padding: 8}}
          numColumns={2}
          showsHorizontalScrollIndicator={false}
          data={this.state.movies}
          renderItem={(movie) => (
            <MovieItem
              column={2}
              movie={movie.item}
              onPress={() =>
                this.props.navigation.navigate('MovieDetail', {
                  movie: movie.item,
                })
              }
            />
          )}
          keyExtractor={(item) => String(item.id)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
