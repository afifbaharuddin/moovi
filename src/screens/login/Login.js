import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import {LoginAction} from '../../redux/actions';

import APIKit, {setClientToken} from '../../api/APIKit';
import {Colors, Mixins, Typography} from '../../styles';
import {Button, TextInput} from '../../components';
import {APIKey} from '../../api/Config';

const mapStateToProps = (state) => ({
  login: state.login,
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
    };
  }

  componentDidMount() {
    if (this.props.login.isLoggedIn) {
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'Tabs'}],
      });
    }
  }

  onLogin = () => {
    const {dispatch} = this.props;
    dispatch(LoginAction.userLogin({username: this.state.username || 'Anon'}));
    setClientToken(APIKey);
    this.props.navigation.reset({
      index: 0,
      routes: [{name: 'Tabs'}],
    });
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={Mixins.container}>
          <Icon name="film" size={56} style={styles.logo} />
          <Text style={{...Typography.TITLE_BIG, alignSelf: 'center'}}>
            Welcome
          </Text>
          <Text style={{...Typography.SUBTITLE, alignSelf: 'center'}}>
            Sign in to continue
          </Text>

          <View style={styles.form}>
            <TextInput
              title="Username"
              onChangeText={(text) => this.setState({username: text})}
              style={styles.inputText}
            />

            <TextInput
              title="Password"
              style={styles.inputText}
              secureTextEntry={true}
              value="testtt"
            />

            <Button text="Sign In" onPress={this.onLogin} />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    marginTop: 120,
    marginBottom: 8,
    color: Colors.PRIMARY,
    alignSelf: 'center',
  },

  form: {
    ...Mixins.boxShadow(),
    margin: 24,
    marginTop: 48,
    paddingHorizontal: 16,
    paddingVertical: 40,
    borderRadius: 12,
  },

  inputText: {
    marginBottom: 32,
  },
});

export default connect(mapStateToProps)(Login);
