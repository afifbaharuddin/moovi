import {USER_LOGIN, USER_LOGOUT} from '../actions/login';

const initialState = {
  isLoggedIn: false,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN: {
      const {username} = action.payload;
      return {
        isLoggedIn: true,
        username,
      };
    }
    case USER_LOGOUT: {
      return {
        isLoggedIn: false,
      };
    }
    default: {
      return state;
    }
  }
};

export default userReducer;
