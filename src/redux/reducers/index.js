import {combineReducers} from 'redux';
import loginReducer from './login';

// We call state in here
export default combineReducers({
  login: loginReducer,
});
