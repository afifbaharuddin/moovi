export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USR_LOGOUT';

export const userLogin = ({username}) => ({
  type: USER_LOGIN,
  payload: {
    username,
  },
});

export const userLogout = () => ({
  type: USER_LOGOUT,
  payload: {},
});
